# Atlassian Confluence in a Docker container


A containerized installation of Atlassian Confluence for use in the Affinityps intranet.

## Build an affinityps/confluence Image
Download contents of affinityps-confluence repo to any linux host with a docker daemon, then use

```
sudo docker build -t affinityps/confluence:5.7 dockerfiles/confluence/.
```


## Provision a Host with Container via Image

Container runs on ports 8090 (http) and 8443 (https) and must be mapped to host port when run.


To provision a dev host on http with the defaults, use:

```
sudo docker run --name confluence -e ENV=prod -v /var/local/atlassian/confluence:/var/local/atlassian/confluence -p 80:8090 -p 443:8443 -t affinityps/confluence:5.7.1
```

## Volumes

Volume mount points are provided for installation and home directories.


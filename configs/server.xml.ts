<Server port="8000" shutdown="SHUTDOWN" debug="0">
    <Service name="Tomcat-Standalone">
        <Connector port="8090"
            connectionTimeout="20000" 
            redirectPort="8443"
            maxThreads="200" minSpareThreads="10"
            enableLookups="false" 
            acceptCount="10" 
            debug="0" 
            URIEncoding="UTF-8" />

        <Connector port="8443"
             protocol="org.apache.coyote.http11.Http11Protocol"
             maxHttpHeaderSize="8192"
             SSLEnabled="true"
             maxThreads="150"
             minSpareThreads="25"
             maxSpareThreads="75"
             enableLookups="false"
             disableUploadTimeout="true"
             acceptCount="100"
             scheme="https"
             secure="true"
             clientAuth="false"
             sslProtocol="TLS"
             useBodyEncodingForURI="true"
             keystoreFile="/usr/local/confluence/security/confluence_keystore.pkcs12"
             keystoreType="PKCS12"
             keystorePass="affinityps"
             truststoreFile="/usr/local/confluence/security/confluence-truststore.jks"
             truststorePass="affinityps"
             truststoreType="JKS"/>

        <Engine name="Standalone" defaultHost="localhost" debug="0">
            <Host name="localhost" debug="0" appBase="webapps" unpackWARs="true" autoDeploy="false">
                <Context path="" 
                    docBase="../confluence" 
                    debug="0" 
                    reloadable="false" 
                    useHttpOnly="true">
                </Context>
            </Host>
        </Engine>
    </Service>
</Server>

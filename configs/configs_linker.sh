#!/bin/bash

# $1 is the container file
# $2 is the configurable version of the file

# Move the container file to the configs dir, make it writeable
# by daemon, and symlink it back to the original container location
mv $1 $2 
chmod 644 $2 
ln -s $2 $1
chmod 644 $1

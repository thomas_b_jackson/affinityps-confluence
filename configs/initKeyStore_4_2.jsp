<%@ page import="com.atlassian.spring.container.ContainerManager" %>
<%@ page import="com.atlassian.confluence.util.PatternLayoutWithStackTrace" %>
<%@ page import="com.atlassian.confluence.util.GeneralUtil" %>
<%@ page import="com.atlassian.confluence.security.trust.KeyPairInitialiser;" %>

<html>
<body>

<%
    boolean doUpgrade = Boolean.valueOf(request.getParameter("doUpgrade")).booleanValue();

    if (doUpgrade)
    {
        KeyPairInitialiser keyPairInitialiser = (KeyPairInitialiser) ContainerManager.getComponent("keyPairInitialiser");

        try
        {
            keyPairInitialiser.initConfluenceKey();
            out.println("<span style=\"color: green\">KEYSTORE table initialised successfully.</span>");
        }
        catch (Exception e)
        {
            out.print("<span style=\"color: red\">Error occurred trying to initialise keystore table:</span> <br/>");
%>
        <pre>
<%
            StringBuffer stacktraceBuffer = new StringBuffer();
            PatternLayoutWithStackTrace.appendStackTrace(stacktraceBuffer, e);
            out.print(GeneralUtil.htmlEncode(stacktraceBuffer.toString()));
%>
        </pre>
<%
        }
    }
    else
    {
%>

<p>
This JSP will initialise your KEYSTORE table. Do you wish to proceed?
</p>
<p>
<form method="GET" action="initKeyStore_4_2.jsp">
    <input type="hidden" name="doUpgrade" value="true"/>
    <input type="submit" value="Proceed"/>
</form>
</p>
<%
    }
%>

</body>
</html>


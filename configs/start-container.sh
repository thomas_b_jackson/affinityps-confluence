#!/bin/bash

# if this is the dev env, copy the simplified urlrewrite.xml into place
if [$ENV = "dev"]; then

	cp $CONFLUENCE_HOME/affinity-configs/urlrewrite.xml ${CONFLUENCE_INSTALL}/confluence/WEB-INF/urlrewrite.xml
	chmod 444 ${CONFLUENCE_INSTALL}/confluence/WEB-INF/urlrewrite.xml

fi

./bin/start-confluence.sh -fg
